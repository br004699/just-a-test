"""Numerical solution of the inertial oscillation equation using
forward-backward time-stepping with Coriolis parameter f. Alex Doyle"""
#dudt = f*v
#dvdt = -f*u

import matplotlib.pyplot as plt # Plotting
import numpy as np # Numerical calculations

def main():
	# Parameters
	f = 1e-4 # Coriolis parameter
	nt = 24 # Number of timesteps
	dt = 2500 # Time step in seconds
	# Initial conditions
	x0 = 0.
	y0 = 1e5
	u0 = 50.
	v0 = 50.
	# Initialise velocity from initial conditions
	u = u0
	v = v0
	# Store all locations for plotting and store initial conditions
	x = np.zeros(nt+1)
	y = np.zeros(nt+1)
	u = np.zeros(nt+1)
	v = np.zeros(nt+1)
	x[0] = x0
	y[0] = y0
	
	# Loop over all timesteps
	for n in range(nt):
		#u += dt*f*v
		#v -= dt*f*u
		#x[n+1] = x[n] + dt*u
		#y[n+1] = y[n] + dt*v
		
		x += dt*u[n-1]
		y -= dt*v[n-1]
		u[n+1] = u[n] + dt*f*v[n] 
		v[n+1] = v[n] - dt*f*u[n]
	
	# Analytic solution for the location as a function of time
	times = np.linspace(0,nt*dt,nt+1)
	xa = x0 + 1/f*(u0*np.sin(f*times) - v0*np.cos(f*times) + v0)
	ya = y0 + 1/f*(u0*np.cos(f*times) + v0*np.sin(f*times) - u0)
	ua = (u0*np.cos(f*times)) + (v0*np.sin(f*times))
	va = -(u0*np.sin(f*times)) + (v0*np.cos(f*times))
	
	"""
	# Plot the solution in comparison to the analytic solution
	plt.plot(xa/1000.,ya/1000.,'-k+',label='analytic')
	plt.plot(x/1000.,y/1000.,'-bo',label='forward-backward')
	#plt.fill(x/1000.,xa/1000.,y/1000.,ya/1000.)
	plt.xlabel('x')
	plt.ylabel('y')
	plt.legend(loc=0)
	plt.show()
	"""
	
	plt.plot(ua,va,'-bo',label='analytic')
	plt.plot(u,v,'-k+',label='forward-forward')
	plt.xlabel('u (m/s)')
	plt.ylabel('v (m/s)')
	plt.legend(loc=0)
	plt.show()
	

# Execute the code
if __name__ == '__main__':
	main()
